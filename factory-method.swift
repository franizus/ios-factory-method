protocol Vehicle {
    func printVehicle()
}

class TwoWheeler : Vehicle { 
    func printVehicle()  { 
        print("I am two wheeler")
    } 
}

class FourWheeler : Vehicle { 
    func printVehicle()  { 
        print("I am four wheeler")
    } 
}; 

class Client { 
    private var vehicle:Vehicle?
    init(type:Int)  { 
        if type == 1 {
            vehicle = TwoWheeler()
        } else if type == 2 {
            vehicle = FourWheeler()
        } else {
            vehicle = nil
        }
    }
  
    func getVehicle() -> Vehicle { 
        return vehicle!
    }
}; 

let client = Client(type: 1)
let vehicle = client.getVehicle()
vehicle.printVehicle()